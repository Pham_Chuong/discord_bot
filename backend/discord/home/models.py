# _*_ coding:utf-8 _*_
from datetime import timedelta
from django.db import models
from django.utils.timezone import now


class User(models.Model):
	id_discord 		= models.CharField(max_length=255, blank=True, null=True)
	user_name 		= models.CharField(max_length=255, blank=True, null=True)
	time_create 	= models.DateTimeField(auto_now_add=True)


class Message(models.Model):
	user_sent 		= models.CharField(max_length=255, blank=True, null=True)
	user_received 	= models.CharField(max_length=255, blank=True, null=True)
	message 		= models.CharField(max_length=255, blank=True, null=True)
	reply                 = models.CharField(max_length=255, blank=True, null=True)
	total_icon 		= models.CharField(max_length=255, blank=True, null=True)
	time_create 	= models.DateTimeField(auto_now_add=True)


class Hashtag(models.Model):
	user_sent 		= models.CharField(max_length=255, blank=True, null=True)
	hashtag 		= models.CharField(max_length=255, blank=True, null=True)
	channel 		= models.CharField(max_length=255, blank=True, null=True)
	time_create 	= models.DateTimeField(auto_now_add=True)
