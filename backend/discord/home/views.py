
# _*_ coding:utf-8 _*_
import json
import csv
from django.db.models import Q, Sum,Count
from math import ceil
from django.db import transaction
from django.views.decorators.csrf import csrf_exempt,ensure_csrf_cookie
from django.middleware.csrf import _get_new_csrf_key
from django.db.models import Prefetch
from django.views.decorators.http import require_http_methods
from datetime import timedelta
from datetime import datetime
from django.core import serializers
from django.db import connection,connections
from _ast import Param
from _mysql import connect
from django.db import IntegrityError
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_headers 
from django.core.cache import cache
import cStringIO as StringIO
import itertools
import sys
from django.core.cache import cache
from operator import itemgetter
from django.db.models import F
from django.shortcuts import render
from django.http import HttpResponse
from home.models import *
reload(sys)
sys.setdefaultencoding('utf-8')



def index(request):
    """
    View function for home page of site.
    """
    return render(
        request,
        'index.html'
    )


def report(request):
	arr_data 	= []
	arr_user 	= []
	c 			= connection.cursor()
	query 		= '''SELECT hashtag,user_sent,count(*) FROM `discord`.home_hashtag group by hashtag,user_sent;'''
	c.execute(query)
	result 		= c.fetchall()
	for x in result:
		if '@' in x[0] or '<' in x[0]:
			continue
		if '@' in x[0] and '<#' in x[0]:
                        continue
		if x[0][0] != '#':
			continue
		if len(x[0]) < 5:
			continue
		obj = {
			'hashtags' : x[0],
			'user_sent' : x[1],
			'total' : x[2],
		}
		arr_data.append(obj)
	print arr_data
	return render(
		request,
		'report.html',{'data': arr_data[:10]}
	)


@csrf_exempt
def post_user_discord(request):
	if request.method == 'POST':
		json_data 		= json.loads(request.body)
		arr_user 		= json_data['arr_user']
		for x in arr_user:
			__user = User.objects.filter(id_discord = x['id_discord']).first()
			if __user is None:
				user = User()
				user.id_discord = x['id_discord']
				user.user_name 	= x['user_name']
				user.save()
		data = json.dumps({
			'error'	: False,
			'msg'	: 'Save successfully' 
		})
		return HttpResponse(data, content_type='application/json', status=200)
	else:
		obj = {
			'msg' 		: 'METHOD NOT ALLOWED',
			'status' 	: 405
		}
		return HttpResponse(json.dumps(obj), content_type='application/json', status=405)



@csrf_exempt
def post_icon_discord(request):
	if request.method == 'POST':
		json_data 		= json.loads(request.body)
		user_sent 		= json_data['user_sent']
		user_name 		= json_data['user_name']
		user_received 	= json_data['user_received']
		user_name_received 	= json_data['user_name_received']
		total 			= json_data['total']
		message 		= json_data['message']
		reply                 = json_data['reply']
		__user = User.objects.filter(id_discord = user_sent).first()
		if __user is None:
			user = User()
			user.id_discord = user_sent
			user.user_name 	= user_name
			user.save()
		__user_r = User.objects.filter(id_discord = user_received).first()
		if __user_r is None:
			user = User()
			user.id_discord = user_received
			user.user_name 	= user_name_received
			user.save()
		
		if int(total) == 0 and  reply is not None and 'flat' not in message:
			print "assssssssssssssss",user_name_received
			ms = Message.objects.filter(user_sent = user_name_received,total_icon = 0).exclude(message = 'flat').order_by('-time_create').first()
			if ms is not None:
				ms.reply = reply if reply is not None else None
				ms.save()

		else: 
			ms = Message()
			ms.user_sent 		= user_name
			ms.user_received 	= user_name_received
			ms.message 		= message
			ms.total_icon 		= total
			ms.save()
			print "dddddddddddddddddddd",user_name_received
			print message
		data = json.dumps({
			'error'	: False,
			'msg'	: 'Save successfully' 
		})
		return HttpResponse(data, content_type='application/json', status=200)
	else:
		obj = {
			'msg' 		: 'METHOD NOT ALLOWED',
			'status' 	: 405
		}
		return HttpResponse(json.dumps(obj), content_type='application/json', status=405)

@csrf_exempt
def get_user_discord(request,user_id):
	if request.method == 'GET':
		__msg = Message.objects.filter(user_received = user_id,message = 'flat').order_by('-time_create')
		c 			= connection.cursor()
		query 		= '''select user_sent,user_received,sum(total_icon) from `discord`.home_message where total_icon != 0 and user_received = {} group by user_received'''.format(user_id)
		print query
		c.execute(query)
		result 		= c.fetchall()
		report = []
		if __msg.count() == 0:
			obj = {
				'datetime' 	: str(datetime.now()),
				'user_rc' 	: 'Empty',
				'total' 	: 'Empty',
				'time_rc' 	: 'Empty',
			}
			report.append(obj)
			data = json.dumps({
				'error'		: False,
				'report' 	: report,
				'user_A'	: None,
				'user_B'	: None
			})
			return HttpResponse(data, content_type='application/json', status=200)
		
		for x in result:
			obj = {
				'datetime' 	: str(datetime.now()),
				'user_rc' 	: x[1],
				'user_st' 	: x[0],
				'total' 	: int(x[2])
			}
			report.append(obj)
		print report
		data = json.dumps({
			'error'		: False,
			'report' 	: report,
			'user_A'	: __msg.first().user_received,
			'user_B'	: __msg.first().user_sent,
			'datetime' 	: str(datetime.now()),
		})
		return HttpResponse(data, content_type='application/json', status=200)
@csrf_exempt
def hashtag_discord(request):
	if request.method == 'POST':
		json_data 		= json.loads(request.body)
		user_sent 		= json_data['user_sent']
		channel 		= json_data['channel']
		hashtag 		= json_data['hashtag']
		ht = Hashtag()
		ht.user_sent 	= user_sent
		ht.hashtag 		= hashtag
		ht.channel 		= channel
		ht.save()
		data = json.dumps({
			'error'	: False,
			'msg'	: 'Save successfully' 
		})
		return HttpResponse(data, content_type='application/json', status=200)
	else:
		obj = {
			'msg' 		: 'METHOD NOT ALLOWED',
			'status' 	: 405
		}
		return HttpResponse(json.dumps(obj), content_type='application/json', status=405)


@csrf_exempt
def report_hashtag_discord(request):
	if request.method == 'GET':
		data 		= []
		arr_user 	= []
		ht = Hashtag.objects.all()
		for x in ht:
			if x.user_sent not in arr_user:
				obj = {
					'user_sent' 	: x.user_sent,
					'hashtag' 		: x.hashtag,
					'channel' 		: x.channel,
					'total' 		: 1
				}
				data.append(obj)
				arr_user.append(x.user_sent)
			else:
				key__ = False
				for item in data:
					if x.user_sent == item['user_sent'] and  x.hashtag == item['hashtag'] and x.channel == item['channel']:
						item['total'] = int(item['total']) +1
						key__ = True
				if key__ is False:
					obj = {
						'user_sent' 	: x.user_sent,
						'hashtag' 		: x.hashtag,
						'channel' 		: x.channel,
						'total' 		: 1
					}
					data.append(obj)


		data = json.dumps({
			'error'	: False,
			'data'	: data 
		})
		return HttpResponse(data, content_type='application/json', status=200)
	else:
		obj = {
			'msg' 		: 'METHOD NOT ALLOWED',
			'status' 	: 405
		}
		return HttpResponse(json.dumps(obj), content_type='application/json', status=405)


@csrf_exempt
def get_user_discord_seek(request,user_id,user_rc):
	if request.method == 'GET':
		__msg = Message.objects.filter(user_received = user_id,user_sent = user_rc,total_icon = 0).exclude(message = 'flat').order_by('-time_create')
		print __msg
		report = []
		if __msg.first() is None:
			obj = {
				'datetime' 	: str(datetime.now()),
				'user_rc' 	: 'Empty',
				'total' 	: 'Empty',
				'time_rc' 	: 'Empty',
			}
			report.append(obj)
			data = json.dumps({
				'error'		: False,
				'report' 	: report,
				'user_A'	: None,
				'user_B'	: None
			})
			return HttpResponse(data, content_type='application/json', status=200)
		msg 	= Message.objects.filter(user_received = user_id,user_sent = user_rc,total_icon = 0).exclude(message = 'flat').order_by('-time_create')
		print msg.query
		report 	= []
		for x in msg:
			if int(x.total_icon) != 0:
				continue
			obj = {
				'datetime' 	: str(datetime.now()),
				'user_rc' 	: x.user_received,
				'total' 	: x.total_icon,
				'msg' 		: x.message,
				'rep' 		: x.reply,
				'time_rc' 	: str(x.time_create),
			}
			report.append(obj)
		data = json.dumps({
			'error'		: False,
			'report' 	: report,
			'user_A'	: __msg.first().user_received,
			'user_B'	: __msg.first().user_sent
		})
		return HttpResponse(data, content_type='application/json', status=200)

@csrf_exempt
def get_gift_user_discord(request,user_id):
	from datetime import datetime, date,timedelta
        dt1 = datetime.now()
        start_of_week = dt1+timedelta(days=0-dt1.weekday())
        end_of_week = dt1+timedelta(days=6-dt1.weekday())
	if request.method == 'GET':
		total_gift 	= 0
		c 			= connection.cursor()
		query 		= '''select sum(total_icon) from home_message where user_sent = %s and date(time_create) >= %s and date(time_create) <= %s'''
		print query
		c.execute(query,[user_id,start_of_week,end_of_week])
		result 		= c.fetchone()
		if result[0] is not None:
			total_gift = int(result[0])
		data = json.dumps({
			'error'		: False,
			'total' 	: total_gift
		})
		return HttpResponse(data, content_type='application/json', status=200)

@csrf_exempt
def change_report_comand(request):
	if request.method == 'POST':
		json_data 		= json.loads(request.body)
		day 			= json_data['day']
		hours 			= json_data['hours']
		try:
			rp_ = Message.objects.get(user_sent = 'report')
		except Message.DoesNotExist:
			rp_ = None
		if rp_ is None:
			rp_ = Message()
			rp_.user_sent 		= 'report'
			rp_.user_received 	= 'report'
			rp_.message 		= day
			rp_.total_icon 		= hours
			rp_.save()
		else:
			rp_.message = day
			rp_.total_icon 		= hours
			rp_.save()
		data = json.dumps({
			'error'		: False,
		})
		return HttpResponse(data, content_type='application/json', status=200)



@csrf_exempt
def get_gift_report_discord(request):
	from datetime import datetime, date,timedelta
	dt1 = datetime.now()
	start_of_week = dt1+timedelta(days=0-dt1.weekday())
	end_of_week = dt1+timedelta(days=6-dt1.weekday())
	if request.method == 'GET':
		arr_rp 		= []
		c 			= connection.cursor()
		query 		= '''select user_received,sum(total_icon) from home_message where date(time_create) >= %s and date(time_create) <= %s group by user_received'''
		print query
		c.execute(query,[start_of_week,end_of_week])
		result 		= c.fetchall()
		for x in result:
			obj = {
				'user'		: x[0],
				'total' 	: x[1]
			}
			arr_rp.append(obj)
		data = json.dumps({
			'error'		: False,
			'data' 	: arr_rp
		})
		return HttpResponse(data, content_type='application/json', status=200)
