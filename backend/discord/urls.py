#! /usr/bin/env python2.7
"""{{ project_name }} URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
    3. https://discordapp.com/developers/applications/me
"""
from django.conf.urls import include, url
from django.contrib import admin
from discord.home.views import *

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^post_user_discord$', post_user_discord),
    url(r'^$', index),
    url(r'^report$', report),
    url(r'^post_icon_discord$', post_icon_discord),
    url(r'^hashtag_discord$', hashtag_discord),
    url(r'^report_hashtag_discord$', report_hashtag_discord),
    url(r'^get_user_discord/(?P<user_id>.*)$', get_user_discord),
    url(r'^get_user_discord_seek/(?P<user_id>.+)/(?P<user_rc>.+)$', get_user_discord_seek),
    url(r'^get_gift_user_discord/(?P<user_id>.*)$', get_gift_user_discord),
    url(r'^change_report_comand$', change_report_comand),
    url(r'^get_gift_report_discord$', get_gift_report_discord),
]
