# These are the dependecies. The bot depends on these to function, hence the name. Please do not change these unless your adding to them, because they can break the bot.
import discord
import asyncio
from discord.ext.commands import Bot
from discord.ext import commands
import platform
import requests
import json
from discord.utils import get

Domain = 'http://127.0.0.1:8000/'

client = Bot(description="Basic Bot Pham", command_prefix="/",pm_help=True)
# Get all channel
def get_all_channel():
	data = []
	for server in client.servers:
		for channel in server.channels:
			obj = {
				'id_channel' : channel.id,
				'name_channel' : str(channel),
			}
			data.append(obj)
	return data

# Get all user
def get_all_user():
	arr_user = []
	for server in client.servers:
	    for member in server.members:
	    	obj = {
	    		'id_discord' : member.id,
	    		'user_name' 	: str(member.server)
	    	}
	    	arr_user.append(obj)
	url = Domain+'post_user_discord'
	data = {
		'arr_user': arr_user
	}
	req = requests.post(url, json.dumps(data), headers={'Content-type': 'application/json'})
	return True

# Save message by bot	
def save_message_by_bot(user_sent,user_name,user_received,user_name_received,total,message):
	url = Domain+'post_icon_discord'
	data = {
		'user_sent' 			: user_sent,
		'user_name' 			: user_name,
		'user_received' 		: user_received,
		'user_name_received' 	: user_name_received,
		'total' 				: total,
		'message' 				: message
	}
	req = requests.post(url, json.dumps(data), headers={'Content-type': 'application/json'})
	return True


# Save hashtag by bot	
def save_hashtag_by_bot(user_sent,channel,hashtag):
	url = Domain+'hashtag_discord'
	data = {
		'user_sent' : str(user_sent),
		'channel' 	: channel,
		'hashtag' 	: hashtag
	}
	req = requests.post(url, json.dumps(data), headers={'Content-type': 'application/json'})
	return True


@client.event
async def on_ready():

	print('Logged in as '+client.user.name+' (ID:'+client.user.id+') | Connected to '+str(len(client.servers))+' servers | Connected to '+str(len(set(client.get_all_members())))+' users')
	print('--------')
	print('Current Discord.py Version: {} | Current Python Version: {}'.format(discord.__version__, platform.python_version()))
	print('--------')
	print('Use this link to invite {}:'.format(client.user.name))
	print('https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=2146958591'.format(client.user.id))
	print('Created by PhamChuong')
	# Get all chanel
	get_all_channel()
	# # Get All user
	get_all_user()
	return await client.change_presence(game=discord.Game(name='PLAYING STATUS HERE')) #This is buggy, let us know if it doesn't work.



@client.event
async def on_message(message):

	if '#' in message.content:
		msg = message.content.split(' ')
		for x in msg:
			if '#' in x:
				save_hashtag_by_bot(str(message.author),str(message.channel),x)
	if message.content == '/report':
		print (message.author)
		url = Domain+'get_user_discord/'+str(message.author)
		req = requests.get(url,headers={'Content-type': 'application/json'})
		print (req.json())
		for x in req.json()['report']:
			await client.send_message(message.channel,"Date: {}".format(x['datetime']))
			await asyncio.sleep(1)
			if x['total'] == 'Empty':
				await client.send_message(message.channel,"Data is empty !")
				continue
			await client.send_message(message.channel,"You are the luckiest person on earth. You have total {} gifts from {} givers".format(x['total'],x['user_rc']))


	msg = message.content.split('<')
	total_icon = 0
	for x in msg:
		if '@' in x:
			user_reciver = x.split('>')[0]
	for x in message.content:
		if x == '❤':
			total_icon += 1

	print (message.content)
	if '/hide' in message.content:
		msg = message.content.split('>')
		for x in msg:
			if '@' not in x:
				__key = x
		print (message.channel)
		user = discord.utils.get(client.get_all_members(), id = user_reciver.replace('@',''))
		for server in client.servers:
			for channel in server.channels:
				if str(channel) == 'Text Channels' or str(channel) == 'Text Channels' or str(channel) == 'Voice Channels' or str(channel) == 'General':
					continue
				print(channel)
				await client.send_message(channel,'User {} received 1 from someone {}'.format('<'+user_reciver+'>',__key))
				await asyncio.sleep(1)
		await client.send_message(user,'Someone sent you {}.'.format(__key))

	if '/secret' in message.content:
		msg = message.content.split('>')
		for x in msg:
			if '@' not in x:
				__key = x
		user = discord.utils.get(client.get_all_members(), id = user_reciver.replace('@',''))
		await client.send_message(message.channel,'User {} received 1 {} from {}'.format('<'+user_reciver+'>',__key,message.author))
		await asyncio.sleep(1)
		await client.send_message(user,'User {} sent you {}.'.format(message.author,__key))

	if '❤' in message.content:
		if '/hide' not in message.content and '/secret' not in message.content:
			user = discord.utils.get(client.get_all_members(), id = user_reciver.replace('@',''))
			save_message_by_bot(str(message.author.id),str(message.author),user.id,str(user),total_icon,str(message.content))
			await client.send_message(message.channel,'User {} received {} icon in channel #{}.'.format('<'+user_reciver+'>',total_icon,message.channel))
			await asyncio.sleep(1)
			await client.send_message(user,'User {} received {} icon in channel {}.'.format('<'+user_reciver+'>',total_icon,message.channel))



@client.event
async def on_reaction_add(reaction, user):
	msg 	= reaction.message
	chat 	= reaction.message.channel
	if str(reaction.emoji) == '👍':
		url = Domain+'get_user_discord/'+str(user.display_name)
		req = requests.get(url,headers={'Content-type': 'application/json'})
		if req.json()['user_A'] is not None:
			user_reciver_gilf = discord.utils.get(client.get_all_members(), id = req.json()['user_A'])
			await asyncio.sleep(1)
			await client.send_message(user_reciver_gilf,'Hi {}, user {} appreciate your feedback'.format('<@'+req.json()['user_A']+'>',str(user_reciver_gilf)))
	
	if str(reaction.emoji) == '😂':
		url = Domain+'get_user_discord/'+str(user.display_name)
		req = requests.get(url,headers={'Content-type': 'application/json'})
		if req.json()['user_A'] is not None:
			user_reciver_gilf_skip = discord.utils.get(client.get_all_members(), id = req.json()['user_A'])
			await asyncio.sleep(1)
			await client.send_message(user_reciver_gilf_skip,'Hi {}, user {} appreciate your feedback. Have a great day!'.format('<@'+req.json()['user_A']+'>',str(user_reciver_gilf_skip)))

client.run('NDE2MDc4MDA0MjYzOTc2OTYw.DW_O2A.euM7EPwnzucZANKrfuYx4x97xyI')
